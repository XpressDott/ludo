<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('login');
});

Auth::routes();
Route::get('login', 'LoginController@LoginForm')->name('login');
Route::post('loginpost', 'LoginController@LoginPost');
Route::get('logout', 'LoginController@logout')->name('logout');
Route::get('password/reset', 'LoginController@showLinkRequestForm')->name('owner.password.request');
Route::post('password/email', 'LoginController@sendResetLinkEmail')->name('owner.password.email');
Route::get('password/reset/{token}', 'LoginController@showResetForm')->name('password.reset.link');
Route::post('reset/password', 'LoginController@reset')->name('owner.password.update');

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => ['auth:backend']], function () {

    Route::get('/admin/dashboard','DashboardController@index')->name('admin.dashboard');
    Route::get('/settings','DashboardController@settings')->name('user.edit');
    Route::post('/settings/update/{id}','DashboardController@settingsUpdate')->name('user.update');
    Route::post('/settings/updatePassword/{id}','DashboardController@updatePassword')->name('user.updatePassword');
    Route::post('/settings/updateLogo','DashboardController@updateLogo')->name('user.updateLogo');
    Route::post('/settings/updateAppName','DashboardController@updateAppName')->name('user.updateAppName');


    Route::resource('players','PlayerController');
    Route::get('/player/create','PlayerController@create');
    Route::get('/player/list','PlayerController@index')->name('player.list');
    Route::get('/player/delete/{id}', 'PlayerController@delete')->name('player.delete');
    Route::get('/player/edit/{id}', 'PlayerController@edit')->name('player.edit');
    Route::post('/player/update/{id}','PlayerController@update')->name('player.update');
    Route::get('/player/detail/{id}','PlayerController@playerDetail')->name('player.detail');
    Route::get('get/player/view','PlayerController@get_player_view')->name('player.get_player_view');


    Route::get('tournament/list','TournamentController@list');
    Route::get('tournament/create','TournamentController@create')->name('create');
    Route::post('tournament/save','TournamentController@save')->name('tournamentSave');
    Route::get('tournament/edit/{id}', 'TournamentController@edit')->name('tournamentEdit');
    Route::get('tournament/delete/{id}', 'TournamentController@delete')->name('tournamentdelete');
    Route::post('tournament/update/{id}', 'TournamentController@update')->name('tournamentUpdate');

    Route::get('banner/list','BannerController@list');
    Route::get('banner/create','BannerController@create')->name('create');
    Route::post('banner/save','BannerController@save')->name('bannerSave');
    Route::get('banner/edit/{id}', 'BannerController@edit')->name('bannerEdit');
    Route::get('banner/delete/{id}', 'BannerController@delete')->name('bannerdelete');
    Route::post('banner/update/{id}', 'BannerController@update')->name('bannerUpdate');

    Route::get('wallet/withdraw/list','WalletController@list')->name('withdraw.list');
    Route::get('wallet/withdraw/edit/{id}', 'WalletController@edit')->name('walletWithdrawEdit');
    Route::get('wallet/withdraw/reject/{id}', 'WalletController@reject')->name('walletWithdrawReject');
    Route::post('wallet/withdraw/update/{id}', 'WalletController@update')->name('walletWithdrawUpdate');
    Route::post('/wallet/modify', 'WalletController@modifyPlayerWallet')->name('modifyPlayerWallet');
    Route::get('/wallet/show/{id}', 'WalletController@getPlayerWallet')->name('getPlayerWallet');
    Route::get('/wallet/showPage', 'WalletController@showPage')->name('showWalletPage');

    Route::get('support/request/list','SupportController@list')->name('support.list');
    Route::get('support/request/edit/{id}', 'SupportController@edit')->name('supportRequestEdit');
    Route::get('support/request/reject/{id}', 'SupportController@reject')->name('supportRequestReject');
    Route::post('support/request/update/{id}', 'SupportController@update')->name('supportRequestUpdate');

    Route::get('report/players','ReportsController@playerReport')->name('report.playersList');
    Route::get('report/support','ReportsController@supportReport')->name('report.support');
    Route::get('report/bannedPlayers','ReportsController@bannedPlayerReport')->name('report.bannedPlayers');  
    Route::get('report/players/withdraw/{id}','ReportsController@withdrawHistory')->name('withdrawHistory');
});


