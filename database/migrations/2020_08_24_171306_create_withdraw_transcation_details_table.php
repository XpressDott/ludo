<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWithdrawTranscationDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('withdraw_transcation_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('player_id');
            $table->integer('wallet_id');
            $table->date('withdraw_request_date')->nullable();
            $table->double('amt_withdraw',10,2);
            $table->date('withdraw_date')->nullable();
            $table->string('payment_type')->nullable();
            $table->string('account_number')->nullable();
            $table->string('ifsc_code')->nullable();
            $table->string('transcation_number')->nullable();
            $table->string('mobile_number')->nullable();
            $table->string('notes')->nullable();
            $table->integer('status')->default(1)->comment('1 - Pending, 2 - Rejected, 3 - Approved, 4 - Completed');
            $table->timestamps();
        });

    
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('withdraw_transcation_details');
    }
}
