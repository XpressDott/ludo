<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        \DB::table('users')->truncate();
        \DB::table('users')->insert(array (
            0 =>
            array (
                'id' => '1',
                'first_name'=> 'Admin',
                'email' => 'admin@yopmail.com',
                'email_verified_at' => '2013-06-11 07:47:40',
                'password' =>  Hash::make('adminadmn'),
                'mobile_no' => '8124948327',
                'user_type' =>'1',
                'status' => '1',
                'created_at' => '2013-06-11 07:47:40',
                'updated_at' => '2013-06-11 07:47:40',
            ),
            1 =>
            array (
                'id' => '2',
                'first_name'=> 'anand',
                'email' => 'anandCustomer1.@yopmail.com',
                'email_verified_at' => '2013-06-11 07:47:40',
                'password' =>  Hash::make('admin123'),
                'mobile_no' => '9845632177',
                'user_type' =>'2',
                'status' => '1',
                'created_at' => '2013-06-11 07:47:40',
                'updated_at' => '2013-06-11 07:47:40',
            ),
            2 =>
            array (
                'id' => '3',
                'first_name'=> 'Colan',
                'email' => 'jithender@gmail.com',
                'email_verified_at' => '2013-06-11 07:47:40',
                'password' =>  Hash::make('admin123'),
                'mobile_no' => '9845632176',
                'user_type' =>'2',
                'status' => '1',
                'created_at' => '2013-06-11 07:47:40',
                'updated_at' => '2013-06-11 07:47:40',
            ),
        ));
        // \DB::table('users')->delete();
 
        
    }
}