@include('header_script')
@include('header_bar')
@include('side_bar')
@include('flash-message')
<body id="app-container" class="menu-default show-spinner">
    <main>
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <h1>Wallet</h1>
                    <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                        <ol class="breadcrumb pt-0">
                            <li class="breadcrumb-item">
                                <a href="#">Home</a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="{{ url('wallet/withdraw/list') }}">Wallet</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">Modify</li>
                        </ol>
                    </nav>
                    <div class="separator mb-5"></div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="card mb-4">
                        <div class="card-body">   
                         <h5 class="mb-4">Change Wallet Amount</h5>                             
                               
                                <form class="form-horizontal" id="wallet_form" method="post" action="{{ route('modifyPlayerWallet') }}">                            
                                @csrf
                                <div class="form-group row">
                                   <div class="col-sm-6">
                                    <label>Choose Player</label>
                                    <select name="player_id" id="selector" class="form-control select2-single">                                   
                                    @foreach($players as $p)
                                        <option value="{{$p->user_id}}">{{$p->user->email}}</option>
                                    @endforeach
                                    </select>

                                </div>
                                </div>

                                <div class="form-group row">
                                <div class="col-12 col-xs-6 mb-9">
                                   <div class="form-group mb-8">
                                        <label>Wallet Amount</label>                                            
                                        <div id="my_slider" style="background-color:purple"></div> 
                                        <div id="my_display" style="font-weight:bold"></div> 
                                        <input type="hidden" id="amount" name="amount" >
                                                                                
                                    </div>
                                </div>
                                </div>  

                                
                                <div class="form-group row">
                                    <div class="col-md-9">
                                    <button type="submit" class="btn btn-primary">Submit </button>
                                    <button type="reset" class="btn btn-default btn-outline">Reset</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        
                    </div>                
                </div>                        
            </div> 
        </div> 
    </main>                            
</body>         
@include('footer')
<script src="{{asset('js/jquery.validate.js')}}"></script>
<script> 

    $('#wallet_form').validate({ 
        rules: {
            player_id: {
                required: true,
            },
            amount:{
                required:true
            }
        }          
    });


  $("#my_slider").slider({ 
      min: 1,
      max : 10000,
      slide: function(event, ui) {       
            $("#my_display").html(ui.value); 
            $('#amount').val(ui.value);
            } 
    }); 
    

 $('#selector').on('change',function(){
     var playerId=$(this).val();
     var sliderVal =  $("#amount").val;
     
    $.ajax({
            url:'/wallet/show/'+playerId,
            dataType: 'JSON',
            type:'get',
            cache:true,
            data: {
              id: playerId
            },
            success:  function (response) {           
            $( "#my_slider" ).slider( "option", "value", response.current_amount );
            $("#my_display").html(response.current_amount);
            $('#amount').val(response.current_amount);
            },
    });  
        
        });
</script>
