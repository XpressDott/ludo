<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BonusWalletTranscationDetail extends Model
{
    protected $table = 'bonus_wallet_transcation_details';
    protected $fillable = [
        'player_id',
        'wallet_id',
        'trounament_id',
        'amt_used',
        'amt_used_date',
        'status'
    ];

    public function player_id()
    {
        return $this->hasOne('App\Models\PlayersDetail', 'id','player_id');
    }

    public function wallet_id()
    {
        return $this->hasOne('App\Models\BonusWalletDetail', 'id','wallet_id');
    }

    public function trounament_id()
    {
        return $this->hasOne('App\Models\Tournament', 'id','trounament_id');
    }
}
