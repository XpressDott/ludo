<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\BonusWalletDetail;
use App\Models\BonusWalletTranscationDetail;
use App\Models\Tournament;
use App\Models\TournamentRegistartion;
use Illuminate\Http\Request;
use App\Models\WithdrawTranscationDetail;
use Carbon\Carbon;
use App\Models\WalletDetail;
use App\Models\WalletTranscationDetails;
use Illuminate\Support\Facades\Validator;

class WalletController extends Controller
{

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function walletAmountLoad(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'player_id' => 'required',
            'loaded_amount' => 'required',
        ]);
        if ($validator->fails())
        {
            return response(['errors'=>$validator->errors()->all()], 422);
        }
        $todayTime  = Carbon::now();

        $walletDetail = WalletDetail::where('player_id',$request->player_id)->first();
        if($walletDetail != null){
            $walletSaveDetail = WalletDetail::where('player_id',$request->player_id)->update([
                'total_amt_load'        => $walletDetail->total_amt_load + $request->loaded_amount,
                'current_amount'        => $walletDetail->current_amount  + $request->loaded_amount,
                'no_of_load'            => $walletDetail->no_of_load + 1 ,
                'last_load_date'        => $todayTime->format("Y-m-d"),
            ]);


            $transcationDetail = new WalletTranscationDetails;
                $transcationDetail->player_id     = $request->player_id;
                $transcationDetail->wallet_id     = $walletDetail->id;
                $transcationDetail->type          = 'add';
                $transcationDetail->use_of        = 'load';
                $transcationDetail->trans_date    = $todayTime->format("Y-m-d");
                $transcationDetail->amount        = $request->loaded_amount;
            $transcationDetail->save();

                $data= $transcationDetail;
            $response = ['status'=>'Success','message'=>'Amount Successfully Added to Wallet','data'=> $data];
            return response($response, 200);
        } else{
            return response(['status'=>'error','message'=>'No Wallet Found'], 422);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function walletAmountWithdraw(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'player_id' => 'required',
            'withdraw_amount' => 'required',
            'withdraw_request_id' => 'required',
        ]);
        if ($validator->fails())
        {
            return response(['errors'=>$validator->errors()->all()], 422);
        }
        $todayTime  = Carbon::now();

        $walletDetail = WalletDetail::where('player_id',$request->player_id)->first();
        if($walletDetail != null){
            $walletSaveDetail = WalletDetail::where('player_id',$request->player_id)->update([
                'total_amt_withdraw'    => $walletDetail->total_amt_withdraw + $request->withdraw_amount,
                'current_amount'        => $walletDetail->current_amount  - $request->withdraw_amount,
                'no_of_withdraw'        => $walletDetail->no_of_withdraw + 1 ,
                'last_withdraw_date'        => $todayTime->format("Y-m-d"),
            ]);

            $walletSaveDetail = WithdrawTranscationDetail::where('id',$request->withdraw_request_id)->update([
                'withdraw_date'    => $todayTime->format("Y-m-d"),
                'status'            => 4,
                'transcation_number' => $request->transcation_number ? $request->transcation_number: '',
                'notes' => $request->notes ? $request->notes: '',
            ]);
            $transcationDetail = new WalletTranscationDetails;
                $transcationDetail->player_id     = $request->player_id;
                $transcationDetail->wallet_id     = $walletDetail->id;
                $transcationDetail->type          = 'sub';
                $transcationDetail->use_of        = 'withdraw';
                $transcationDetail->trans_date    = $todayTime->format("Y-m-d");
                $transcationDetail->amount        = $request->withdraw_amount;
            $transcationDetail->save();

            $data= $transcationDetail;

            $response = ['status'=>'Success','message'=>'Amount Successfully Withdraw from Wallet','data' => $data];
            return response($response, 200);
        } else{
            return response(['status'=>'error','message'=>'No Wallet Found'], 422);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function walletGameLoad(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'player_id' => 'required',
            'amount' => 'required',
        ]);
        if ($validator->fails())
        {
            return response(['errors'=>$validator->errors()->all()], 422);
        }
        $todayTime  = Carbon::now();

        $walletDetail = WalletDetail::where('player_id',$request->player_id)->first();
            if($walletDetail != null){
                $walletSaveDetail = WalletDetail::where('player_id',$request->player_id)->update([
                    'total_amt_withdraw'    => $walletDetail->total_amt_withdraw + $request->amount,
                    'current_amount'        => $walletDetail->current_amount  - $request->amount,
                    'no_of_withdraw'        => $walletDetail->no_of_withdraw + 1 ,
                    'last_withdraw_date'        => $todayTime->format("Y-m-d"),
            ]);


            $transcationDetail = new WalletTranscationDetails;
                $transcationDetail->player_id     = $request->player_id;
                $transcationDetail->wallet_id     = $walletDetail->id;
                $transcationDetail->type          = 'sub';
                $transcationDetail->use_of        = 'game';
                $transcationDetail->trans_date    = $todayTime->format("Y-m-d");
                $transcationDetail->amount        = $request->amount;
            $transcationDetail->save();

            $data= $transcationDetail;
            $response = ['status'=>'Success','message'=>'Amount Successfully Load from Wallet','data' => $data];
            return response($response, 200);
        } else{
            return response(['status'=>'error','message'=>'No Wallet Found'], 422);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function walletHistory(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'player_id' => 'required',
        ]);
        if ($validator->fails())
        {
            return response(['errors'=>$validator->errors()->all()], 422);
        }

        $walletWithdraw = WalletTranscationDetails::where('player_id',$request->player_id)->get();
        if($walletWithdraw != null){
            $response = ['status'=>'Success','message'=>'Request Listed Successfully','data' =>$walletWithdraw];
            return response($response, 200);
        } else{
            return response(['status'=>'error','message'=>'No Wallet Found'], 422);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function walletWithdrawRequest(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'player_id' => 'required',
        ]);
        if ($validator->fails())
        {
            return response(['errors'=>$validator->errors()->all()], 422);
        }

        $walletWithdraw = WithdrawTranscationDetail::where('player_id',$request->player_id)->get();
        if($walletWithdraw != null){
            $response = ['status'=>'Success','message'=>'Request Listed Successfully','data' =>$walletWithdraw];
            return response($response, 200);
        } else{
            return response(['status'=>'error','message'=>'No Wallet Found'], 422);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function withdrawRequestAdd(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'player_id' => 'required',
            'amt_withdraw' => 'required',
        ]);
        if ($validator->fails())
        {
            return response(['errors'=>$validator->errors()->all()], 422);
        }
        $todayTime  = Carbon::now();

        $walletDetail = WalletDetail::where('player_id',$request->player_id)->first();

        if($walletDetail != null){
            $transcationDetail = new WithdrawTranscationDetail;
                $transcationDetail->player_id                = $request->player_id;
                $transcationDetail->wallet_id                = $walletDetail->id;
                $transcationDetail->withdraw_request_date    = $todayTime->format("Y-m-d");
                $transcationDetail->amt_withdraw             = $request->amt_withdraw;
                $transcationDetail->payment_type             = $request->payment_type;
                $transcationDetail->account_number           = $request->account_number ? $request->account_number  :'';
                $transcationDetail->ifsc_code                = $request->ifsc_code ? $request->ifsc_code  :'';
                $transcationDetail->mobile_number            = $request->mobile_number ? $request->mobile_number :'';
                $transcationDetail->status                   = 1;
            $transcationDetail->save();


            $response = ['status'=>'Success','message'=>'Request Sent'];
            return response($response, 200);
        } else{
            return response(['status'=>'error','message'=>'No Wallet Found'], 422);
        }

    }


    public function bonuswalletAmountLoad(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'player_id' => 'required',
            'loaded_amount' => 'required',
        ]);
        if ($validator->fails())
        {
            return response(['errors'=>$validator->errors()->all()], 422);
        }
        $todayTime  = Carbon::now();

        $walletDetail = BonusWalletDetail::where('player_id',$request->player_id)->first();
        $tournment_id= TournamentRegistartion::where('player_id',$request->player_id)->first();
        $tournments= Tournament::where('id',$tournment_id->tournament_id)->first();
        if($walletDetail != null){
            $walletSaveDetail = BonusWalletDetail::where('player_id',$request->player_id)->update([
                'total_amt_added'        => $walletDetail->total_amt_load + $request->loaded_amount,
                'current_amount'        => $walletDetail->current_amount  + $request->loaded_amount,
                'last_added_date'        => $todayTime->format("Y-m-d"),
            ]);


            $transcationDetail = new BonusWalletTranscationDetail();
                $transcationDetail->player_id     = $request->player_id;
                $transcationDetail->wallet_id     = $walletDetail->id;
                $transcationDetail->trounament_id     = $tournments->id;
                $transcationDetail->amt_used          =$request->loaded_amount;
                $transcationDetail->amt_used_date    = $todayTime->format("Y-m-d");
            $transcationDetail->save();

                $data= $transcationDetail;
            $response = ['status'=>'Success','message'=>'Amount Successfully Added to Bonus Wallet','data'=> $data];
            return response($response, 200);
        } else{
            return response(['status'=>'error','message'=>'No Bonus Wallet Found'], 422);
        }
    }


    public function bonuswalletAmountWithdraw(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'player_id' => 'required',
            'withdraw_amount' => 'required',
            'withdraw_request_id' => 'required',
        ]);
        if ($validator->fails())
        {
            return response(['errors'=>$validator->errors()->all()], 422);
        }
        $todayTime  = Carbon::now();

        $walletDetail = BonusWalletDetail::where('player_id',$request->player_id)->first();
        $tournment_id= TournamentRegistartion::where('player_id',$request->player_id)->first();
        $tournments= Tournament::where('id',$tournment_id->tournament_id)->first();

        if($walletDetail != null){
            $walletSaveDetail = BonusWalletDetail::where('player_id',$request->player_id)->update([
                'total_amt_used'    => $walletDetail->total_amt_added + $request->total_amt_used,
                'current_amount'        => $walletDetail->current_amount  - $request->total_amt_used,
                'last_used_date'        => $todayTime->format("Y-m-d"),
            ]);


            $transcationDetail = new BonusWalletTranscationDetail;
            $transcationDetail->player_id     = $request->player_id;
            $transcationDetail->wallet_id     = $walletDetail->id;
            $transcationDetail->trounament_id     = $tournments->id;
            $transcationDetail->amt_used          =$request->withdraw_amount;
            $transcationDetail->amt_used_date    = $todayTime->format("Y-m-d");
            $transcationDetail->save();

            $data= $transcationDetail;

            $response = ['status'=>'Success','message'=>'Amount Successfully Withdraw from Wallet','data' => $data];
            return response($response, 200);
        } else{
            return response(['status'=>'error','message'=>'No Wallet Found'], 422);
        }
    }


}
