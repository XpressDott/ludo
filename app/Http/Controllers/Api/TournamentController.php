<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Tournament;
use Carbon\Carbon;
use App\Models\TournamentRegistartion;
use Illuminate\Support\Facades\Validator;

class TournamentController extends Controller
{
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function tournamentList()
    {
        $tournament = Tournament::get();
        $todayTime  = Carbon::now();
    //     $todayTime  = Carbon::createFromFormat('Y-m-d H:i:s', $some_date, 'UTC')
    // ->setTimezone('America/Los_Angeles')

        // foreach ($tournament as $key => $value) {
        //     $start = $todayTime->format("Y-m-d").' '.$value->start_time;
        //     $startTime = Carbon::createFromFormat('Y-m-d H:i:s', $start);
        //     $end = Carbon::createFromFormat('Y-m-d H:i:s', $start)->addMinutes($value->tournament_interval);

        //     $totalDuration =  $todayTime->diff($end)->format('%H:%I:%S');
        //     // dd($startTime,$end,$totalDuration);

        //     $data[$key]['id'] = $value->id;
        //     $data[$key]['tournament_name'] = $value->tournament_name;
        //     $data[$key]['bet_amount'] = $value->bet_amount;
        //     $data[$key]['commission'] = $value->commission;
        //     $data[$key]['no_players'] = $value->no_players;
        //     $data[$key]['start_time'] = $value->start_time;
        //     $data[$key]['end_time'] = $value->end_time;
        //     $data[$key]['tournament_interval'] = $value->tournament_interval;
        //     $data[$key]['pending_time'] = $totalDuration;
        //     $data[$key]['status'] = $value->tournament_interval;


        // }

        $response = ['status'=>'Success','message'=>'Tournament Listed Successfully','data' => $tournament];
        return response($response, 200);
    }

    /**
     * Show the form for Update a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function tournamentUpdate(Request $request)
    {
        $todayTime = Carbon::now();
        $end_time = Carbon::createFromFormat('Y-m-d H:i:s', $todayTime)->addMinutes(30);



       // dd($todayTime,$todayTime->format("H:i:s"));

        $tournament = Tournament::where('id', $request->id)->update([
            'end_time'    => $end_time->format("H:i:s"),
            'start_time'    => $todayTime->format("H:i:s"),
            'status'      => 3,
        ]);
        $tournament = Tournament::where('id',$request->id)->first();
        $response = ['status'=>'Success','message'=>'Tournament Completed ','data' => $tournament];
        return response($response, 200);
    }

     /**
     * Show the form for Update a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function tournament15Min()
    {
        $todayTime = Carbon::now();
        $start = $todayTime->format("Y-m-d H:i:s");
        $end = Carbon::createFromFormat('Y-m-d H:i:s', $start)->addMinutes(1);
        $end_time = Carbon::createFromFormat('Y-m-d H:i:s', $start)->addMinutes(15);

        $tournament = Tournament::where('tournament_interval', 15)->update([
            'end_time'    => $end_time,
            'start_time'    => $end,
        ]);
        $tournament = Tournament::where('tournament_interval', 15)->get();

        foreach($tournament as $tour)
        {
            $register= TournamentRegistartion::where('tournament_id',$tour->id)->delete();
        }

        $response = ['status'=>'Success','message'=>'Tournament Completed ','data' => $tournament];
        return response($response, 200);
    }
    /**
     * Show the form for Update a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function tournament30Min(Request $request)
    {
        $todayTime = Carbon::now();
        $start = $todayTime->format("Y-m-d H:i:s");
        $end = Carbon::createFromFormat('Y-m-d H:i:s', $start)->addMinutes(1);
        $end_time = Carbon::createFromFormat('Y-m-d H:i:s', $start)->addMinutes(30);

        $tournament = Tournament::where('tournament_interval', 30)->update([
            'end_time'    => $end_time,
            'start_time'  => $end,
        ]);
        $tournament = Tournament::where('tournament_interval', 30)->get();

        foreach($tournament as $tour)
        {
            $register= TournamentRegistartion::where('tournament_id',$tour->id)->delete();

        }


        $response = ['status'=>'Success','message'=>'Tournament Completed ','data' => $tournament];
        return response($response, 200);
    }

    /**
     * Show the form for Update a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function tournamentRegistartion(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'player_id' => 'required',
            'tournament_id' => 'required',
        ]);
        if ($validator->fails())
        {
            return response(['errors'=>$validator->errors()->all()], 422);
        }

        $todayTime= Carbon::now();
        $tour_registration = TournamentRegistartion::where('register_date',$todayTime->format("Y-m-d"))->orderBy('id', 'DESC')->first();
        if ($tour_registration == null) {
            $type = 'creater';
        } else {
            if($tour_registration->type == 'creater'){
                $type = 'joiner';
            } else{
                $type = 'creater';
            }
        }

        $tournament = TournamentRegistartion::create([
            'player_id' => $request->player_id,
            'tournament_id'=> $request->tournament_id,
            'type'=> $type,
            'register_date'    => $todayTime->format("Y-m-d"),
        ]);

        $response = ['status'=>'Success','message'=>'Tournament Registered '];
        return response($response, 200);
    }

    public function tournamentRegistartionList(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'player_id' => 'required',
        ]);
        if ($validator->fails())
        {
            return response(['errors'=>$validator->errors()->all()], 422);
        }
        $todayTime = Carbon::now();
        $tournament = TournamentRegistartion::where('player_id',$request->player_id)->where('register_date',$todayTime->format("Y-m-d"))->get();
        $response = ['status'=>'Success','message'=>'Tournament Registered ','data'=>$tournament];
        return response($response, 200);
    }

    public function tournamentRegistartionOverallList(Request $request)
    {
        $todayTime = Carbon::now();

        $tournament = TournamentRegistartion::where('register_date',$todayTime->format("Y-m-d"))->get();
        $response = ['status'=>'Success','message'=>'Tournament Registered List','data'=>$tournament];
        return response($response, 200);
    }

    public function tournamentRegistartionCount(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'tournament_id' => 'required',
        ]);
        if ($validator->fails())
        {
            return response(['errors'=>$validator->errors()->all()], 422);
        }

        $tournament = TournamentRegistartion::where('tournament_id',$request->tournament_id)->get()->count();
        $response = ['status'=>'Success','message'=>'Tournament Registered Count','data'=>$tournament];
        return response($response, 200);
    }
}
