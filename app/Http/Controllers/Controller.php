<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Config;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function sendAndroidPushNotification( $message, $deviceToken,$key)
    {
        // start
        $apiAccessKey=Config::get('pushcredential.android');

        // $apiAccessKey="AAAAQvkh-NU:APA91bEcbb3oTpsaEacUDete4tlKzVf26hLHQTJfTfV3dmZVkgQX8yMSe4d84pIFWajtxdAEWAcR_PpDDie8GgJl4gIcg1xjoq14qG_EQgpEAvYCcyTIzDrS53OUWLthMgzlqflFC8-T";
        
        #prep the bundle
        $msgJson = array('body' => $message, 'title'	=> 'Ludo app', 'icon'	=> 'myicon', 'sound' => 'mySound', 'key' => $key);
        $fields = array('to' => $deviceToken, 'data'	=> $msgJson);
        $headers = array('Authorization: key=' . $apiAccessKey,'Content-Type: application/json');
        #Send Reponse To FireBase Server
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        curl_close($ch);
        // dd($result);
        return;
    }

}
